# Declare variable

s = "HELLO"
# print(s)

# length of s
length = len(s)
# print("Length is {}".format(length))

# Access charecter H
char_H = s[0]
print(char_H)

char_O = s[4]
print(char_O)

char_L = s[3]
print(char_L)

char_LL = s[2:4]
print(char_LL)

char_HEL = s[0:3]
print(char_HEL)

char_HELs = s[:3]
print(char_HELs)

name = "Hello, Sotheara"
print(name[7])


print("=================")

wellcome = s + " Python"
print(wellcome)

p = "Programming"
l = list(p)
print(p)
print(l)
l[1] = 'R'
print(l)
# Hello
new_string = 'H'.join(l)
print(new_string)

print("=================")
s = "HELLO"

# replace HELLO -> HALO
s = s.replace('EL', 'A')  # HALO
print(s)

# convert list to string
l = ['h', 'i']
s = "".join(l)
print(s)

# concatinate sptring
school = 'Putisastra'
name = 'Vuthy'
greeting = 'Wellcome {} to {}'.format(name, school)
greeting_2 = 'Wellcome %s to %s' % (name, school)
print(greeting_2)
